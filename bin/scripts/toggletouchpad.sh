#!/bin/bash

if [ -e '/usr/bin/synclient' ]
then
    enable=$(synclient -l | grep -c 'TouchpadOff.*=.*0')
    if [ $enable -eq 1 ]; then
	synclient TouchpadOff=0
    else
	synclient TouchpadOff=1
    fi
fi
